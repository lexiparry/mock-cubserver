#!/bin/sh

JVM_DNS_TTL=${1:-30}
JVM_GC_THREADS=${2:-2}

# JVM properties/settings provided via JAVA_OPTS will override those specified
# earlier on the command line.
exec java \
    -server \
    -Djava.net.preferIPv4Stack=true \
    -Djava.io.tmpdir="${TMPDIR:-/dev/shm}" \
    -Xmx128m -Xms128m \
    -XX:+UnlockExperimentalVMOptions \
    -XX:+UseCGroupMemoryLimitForHeap \
    -XX:+ScavengeBeforeFullGC \
    -XX:+CMSScavengeBeforeRemark \
    -XX:ParallelGCThreads=${JVM_GC_THREADS} \
    -XX:+UseConcMarkSweepGC \
    -XX:+CMSParallelRemarkEnabled \
    -XX:+UseCMSInitiatingOccupancyOnly \
    -XX:CMSInitiatingOccupancyFraction=70 \
    -Dsun.net.inetaddr.ttl=${JVM_DNS_TTL} \
    -Djava.net.preferIPv4Stack=true \
    -Djava.io.tmpdir="${TMPDIR:-/dev/shm}" \
    $JAVA_OPTS \
    -jar /cubserver-*.jar \
    "$@"
