#!/usr/bin/env bash

docker stop kms-cubserver
docker rm kms-cubserver
docker rmi lexipol/kms-cubserver:0.0.1
gradle clean localDev
docker build -t lexipol/kms-cubserver:0.0.1 docker/src
docker run -d --name kms-cubserver --memory="220m" --memory-swap="400m" -p 9071:9071 --network lexipol-net lexipol/kms-cubserver:0.0.1
docker logs -f kms-cubserver
