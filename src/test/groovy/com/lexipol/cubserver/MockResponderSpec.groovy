package com.lexipol.cubserver

import spock.lang.Specification
import spock.lang.Unroll

class MockResponderSpec extends Specification {

    @Unroll
    def "extract out #username from body #body"() {
        expect:
        MockResponder.userName(body) == username

        where:
        body                             || username
        "username=kms6_sa%40lexipol.com" || "kms6_sa"
        "usekms6_sa%40lexipol.com"       || "MISSING_DATA"
        "usekms6_salexipol.com"          || "MISSING_DATA"
        "username=kms6_sal.com"          || "MISSING_DATA"
    }

    @Unroll
    def "reading resource file for user #name and action #action "() {
        expect:
        data == MockResponder.response(name, action)

        where:
        name    | action   || data
        "test"  | "lookup" || '{"lookup":null}'
        "miss"  | "login"  || '{}'
        "empty" | "lookup" || ''
    }

    @Unroll
    def "basic login is working for #body "() {
        given:
        MockResponder mockResponder = new MockResponder()

        expect:
        mockResponder.login(body) == resp

        where:
        body                          || resp
        'username=test%40lexipol.com' || '{"login":null}'
    }

    @Unroll
    def "basic lookup is working for #body"() {
        given:
        MockResponder mockResponder = new MockResponder()

        expect:
        mockResponder.lookup(body) == resp

        where:
        body                          || resp
        'username=test%40lexipol.com' || '{"lookup":null}'

    }

}
