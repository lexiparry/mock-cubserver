package com.lexipol.cubserver;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/v1")
public final class MockResponder {
    private static final String USERNAME_KEY = "username=";
    private static final String AT_SIGN = "%40";
    private static final Logger LOGGER =
            LoggerFactory.getLogger(MockResponder.class);

    private static String userName(final String body) {
        String result = "MISSING_DATA";
        if (StringUtils.contains(body, USERNAME_KEY)
                && StringUtils.contains(body, AT_SIGN)) {
            result = StringUtils
                    .substring(body, StringUtils.indexOf(body, USERNAME_KEY)
                                    + USERNAME_KEY.length(),
                            StringUtils.indexOf(body, AT_SIGN));
        }
        return result;
    }

    private static String response(final String userName, final String action) {
        String data = "{}";
        final String fileName = new StringBuilder()
                .append(userName).append("_").append(action)
                .append(".json").toString();
        try {
            try (InputStream input = new ClassPathResource(fileName).getInputStream()) {
                data = new BufferedReader(
                        new InputStreamReader(input, StandardCharsets.UTF_8))
                        .lines().collect(Collectors.joining("\n"));
            }
        } catch (IOException fnf) {
            LOGGER.error("File {} not found to return as response ", fileName, fnf);
        }
        return data;
    }

    @PostMapping(path = "/user/login", produces = MediaType.APPLICATION_JSON_VALUE)
    public String login(@RequestBody final String body) {
        LOGGER.debug("Request {}", body);
        final String resp = response(userName(body), "login");
        LOGGER.debug("Response {}", resp);
        return resp;
    }

    @PostMapping(path = "/user/lookup", produces = MediaType.APPLICATION_JSON_VALUE)
    public String lookup(@RequestBody final String body) {
        LOGGER.debug("Request {}", body);
        final String resp = response(userName(body), "lookup");
        LOGGER.debug("Response {}", resp);
        return resp;

    }

}
