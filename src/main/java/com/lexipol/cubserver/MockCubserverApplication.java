package com.lexipol.cubserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockCubserverApplication {

    public static void main(final String[] args) {
        SpringApplication.run(MockCubserverApplication.class, args);
    }
}
